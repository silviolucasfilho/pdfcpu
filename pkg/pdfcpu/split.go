package pdfcpu

import (
	"bitbucket.org/silviolucasfilho/pdfcpu/pkg/log"
	"github.com/pkg/errors"
	"io"
	"os"
	"sort"
)

// SplitData represents the PDF context and two information that can be stripped from the
type SplitData struct {
	fileIn       string
	ctx          *Context
	data         []*StreamDict
	splitLength  int64
	inMemory     bool
	bufferPos    int64
	filePos      int64
	seekReader   io.ReadSeeker
	currentChunk int
}

// BufferLen returns the length of the buffer to read all file without the assets
func (s *SplitData) BufferLen() (n int64) {
	return s.ctx.Read.FileSize - s.splitLength
}

func (s *SplitData) DataLen() int {
	return len(s.data)
}

func (s *SplitData) GetData(i int) *StreamDict {
	return s.data[i]
}

func (s *SplitData) GetRawData(i int) ([]byte, error) {
	sd := s.data[i]
	raw := sd.Raw
	if raw != nil {
		return raw, nil
	}
	if _, err := s.seekReader.Seek(sd.StreamOffset, io.SeekStart); err != nil {
		return nil, err
	}
	buff := make([]byte, *sd.StreamLength)
	n, err := s.seekReader.Read(buff)
	if err != nil {
		return nil, err
	}
	if int64(n) != *sd.StreamLength {
		return nil, errors.New("invalid bytes read from " + s.fileIn)
	}
	return buff, nil
}

func (s *SplitData) Close() error {
	if !s.inMemory {
		return s.seekReader.(*os.File).Close()
	}
	return nil
}

func (s *SplitData) Reset() error {

	if !s.inMemory {
		file, err := os.Open(s.fileIn)
		if err != nil {
			return err
		}
		s.seekReader = file
	} else {
		s.ctx.Read.rs.Seek(0, 0)
		s.seekReader = s.ctx.Read.rs
	}

	s.bufferPos = int64(0)
	s.filePos = int64(0)
	s.currentChunk = 0

	return nil
}

// ReadAll read the file completely inside a buffer
//
// Deprecated: this methods will not work for very large files
func (s *SplitData) ReadAll() ([]byte, error) {
	/**
	 * offset 0
	 * --- data ---
	 * offset data[0].StreamOffset
	 * --- asset ---
	 * offset data[0].StreamOffset + data[0].StreamLength
	 * --- data ---
	 * offset data[1].StreamOffset
	 * --- asset ---
	 * offset data[1].StreamOffset + data[1].StreamLength
	 * ...
	 * --- data ---
	 * offset cts.FileSize = EOF
	 */

	var seekReader io.ReadSeeker

	if !s.inMemory {
		file, err := os.Open(s.fileIn)

		if err != nil {
			return nil, err
		}

		defer file.Close()
		seekReader = file
	} else {
		seekReader = s.ctx.Read.rs
	}

	p := make([]byte, s.BufferLen())

	bufferLen := int64(len(p))
	if bufferLen < s.BufferLen() {
		return nil, errors.New("invalid buffer size")
	}
	bufferPos := int64(0)
	filePos := int64(0)
	for currentChunk := 0; len(s.data) > currentChunk; currentChunk++ {
		lengthReader := s.data[currentChunk].StreamOffset - filePos
		log.Info.Println("Reading range => ", filePos, s.data[currentChunk].StreamOffset)
		r := io.LimitReader(seekReader, lengthReader)
		i, err := r.Read(p[bufferPos:])
		if err != nil {
			return nil, err
		}
		bufferPos += int64(i)
		seekPos := s.data[currentChunk].StreamOffset + *s.data[currentChunk].StreamLength
		_, err = seekReader.Seek(seekPos, io.SeekStart)
		if err != nil {
			return nil, err
		}
		filePos = seekPos
	}
	if bufferPos < bufferLen {
		log.Info.Println("Reading last range => ", filePos, s.ctx.Read.FileSize)
		i, err := seekReader.Read(p[bufferPos:])
		if err != nil {
			return nil, err
		}
		bufferPos += int64(i)
		filePos += int64(i)
	}

	if bufferPos != bufferLen {
		return nil, errors.New("Invalid buffer pointer at the end")
	}

	if filePos != s.ctx.Read.FileSize {
		return nil, errors.New("Invalid file pointer at the end")
	}

	return p, nil
}

// Read the pdf without the assets bytes directly in the buffer passed as argument
func (s *SplitData) Read(p []byte) (int, error) {

	/**
	 * offset 0
	 * --- data ---
	 * offset data[0].StreamOffset
	 * --- asset ---
	 * offset data[0].StreamOffset + data[0].StreamLength
	 * --- data ---
	 * offset data[1].StreamOffset
	 * --- asset ---
	 * offset data[1].StreamOffset + data[1].StreamLength
	 * ...
	 * --- data ---
	 * offset cts.FileSize = EOF
	 */

	bufferLen := len(p)
	bufferPos := 0
	oneRead := false
	for currentChunk := s.currentChunk; len(s.data) > currentChunk; currentChunk++ {
		tempBufferLength := int(s.data[currentChunk].StreamOffset - s.filePos)
		if tempBufferLength > bufferLen {
			tempBufferLength = bufferLen
			oneRead = true
		}
		log.Info.Println("Reading range => ", s.filePos, s.data[currentChunk].StreamOffset)
		r := io.LimitReader(s.seekReader, int64(tempBufferLength))
		i, err := r.Read(p[bufferPos:])
		bufferLen -= i
		bufferPos += i
		if err != nil {
			return bufferPos, err
		}
		s.filePos += int64(i)
		if oneRead {
			s.currentChunk = currentChunk
			return bufferPos, nil
		}
		seekPos := s.data[currentChunk].StreamOffset + *s.data[currentChunk].StreamLength
		_, err = s.seekReader.Seek(seekPos, io.SeekStart)
		if err != nil {
			return bufferPos, err
		}
		s.filePos = seekPos
	}
	// we reach the end of chunks
	s.currentChunk = len(s.data)
	if bufferPos < len(p) {
		log.Info.Println("Reading last range => ", s.filePos, s.ctx.Read.FileSize)
		i, err := s.seekReader.Read(p[bufferPos:])
		if err != nil {
			return i, err
		}
		bufferPos += i
		s.filePos += int64(i)
	}

	return bufferPos, nil
}

// SplitDocument extracts fonts and images from one PDF file
func SplitDocument(fileIn string) (*SplitData, error) {

	log.Info.Println("Processing file =>", fileIn)
	ctx, err := readAndOptimizePdf(fileIn, NewDefaultConfiguration())
	if err != nil {
		return nil, err
	}

	// select all pages
	m := IntSet{}
	for i := 1; i <= ctx.PageCount; i++ {
		m[i] = true
	}

	pdfStruct := SplitData{fileIn: fileIn, ctx: ctx}

	log.Info.Println("Extracting images for file", fileIn)

	pdfStruct.splitLength = 0

	for _, io := range ctx.Optimize.ImageObjects {

		if io != nil {
			pdfStruct.data = append(pdfStruct.data, io.ImageDict)
			pdfStruct.splitLength += *io.ImageDict.StreamLength
		}
	}

	log.Info.Println("Extracting fonts for file", fileIn)
	for objNr, _ := range ctx.Optimize.FontObjects {

		fo, sd, err := ExtractAllFontData(ctx, objNr)
		if err != nil {
			return nil, err
		}

		if fo != nil {
			pdfStruct.data = append(pdfStruct.data, sd)
			pdfStruct.splitLength += *sd.StreamLength
		}

	}

	sort.Slice(pdfStruct.data, func(i, j int) bool {
		return pdfStruct.data[i].StreamOffset < pdfStruct.data[j].StreamOffset
	})

	pdfStruct.inMemory = false
	pdfStruct.Reset()
	return &pdfStruct, nil
}

// SplitDocumentStreaming extracts fonts and images from one PDF streaming of bytes
func SplitDocumentStreaming(rs io.ReadSeeker, fileIn string, fileSize int64) (*SplitData, error) {

	log.Info.Println("Processing file =>", fileIn)
	ctx, err := readAndOptimizePdfStreaming(rs, fileIn, fileSize, NewDefaultConfiguration())
	if err != nil {
		return nil, err
	}

	// select all pages
	m := IntSet{}
	for i := 1; i <= ctx.PageCount; i++ {
		m[i] = true
	}

	pdfStruct := SplitData{fileIn: fileIn, ctx: ctx}

	log.Info.Println("Extracting images for file", fileIn)

	pdfStruct.splitLength = 0

	for _, io := range ctx.Optimize.ImageObjects {

		if io != nil {
			pdfStruct.data = append(pdfStruct.data, io.ImageDict)
			pdfStruct.splitLength += *io.ImageDict.StreamLength
		}
	}

	log.Info.Println("Extracting fonts for file", fileIn)
	for objNr, _ := range ctx.Optimize.FontObjects {

		fo, sd, err := ExtractAllFontData(ctx, objNr)
		if err != nil {
			return nil, err
		}

		if fo != nil {
			pdfStruct.data = append(pdfStruct.data, sd)
			pdfStruct.splitLength += *sd.StreamLength
		}

	}

	sort.Slice(pdfStruct.data, func(i, j int) bool {
		return pdfStruct.data[i].StreamOffset < pdfStruct.data[j].StreamOffset
	})

	pdfStruct.inMemory = true
	pdfStruct.Reset()
	return &pdfStruct, nil
}

func readPdf(fileIn string, config *Configuration) (ctx *Context, err error) {

	log.Info.Println("Reading pdf context for", fileIn)
	ctx, err = ReadFile(fileIn, config)
	if err != nil {
		return nil, err
	}
	//log.Info.Println("Validating XRefTable for %s", fileIn)
	//err = validate.XRefTable(ctx.XRefTable)
	//if err != nil {
	//	return nil, err
	//}

	return ctx, nil
}

func readAndOptimizePdf(fileIn string, config *Configuration) (ctx *Context, err error) {

	ctx, err = readPdf(fileIn, config)
	if err != nil {
		return nil, err
	}

	log.Info.Println("Optimizing XRefTable for", fileIn)
	err = OptimizeXRefTable(ctx)
	if err != nil {
		return nil, err
	}
	return ctx, nil
}

func readPdfStreaming(rs io.ReadSeeker, fileIn string, fileSize int64, config *Configuration) (ctx *Context, err error) {

	log.Info.Println("Reading pdf context for", fileIn)
	ctx, err = Read(rs, fileIn, fileSize, config)
	if err != nil {
		return nil, err
	}
	//log.Info.Println("Validating XRefTable for %s", fileIn)
	//err = validate.XRefTable(ctx.XRefTable)
	//if err != nil {
	//	return nil, err
	//}

	ctx.Read.rs.Seek(0, 0)
	return ctx, nil
}

func readAndOptimizePdfStreaming(rs io.ReadSeeker, fileIn string, fileSize int64, config *Configuration) (ctx *Context, err error) {

	ctx, err = readPdfStreaming(rs, fileIn, fileSize, config)
	if err != nil {
		return nil, err
	}

	log.Info.Println("Optimizing XRefTable for", fileIn)
	err = OptimizeXRefTable(ctx)
	if err != nil {
		return nil, err
	}
	return ctx, nil
}

// ReadXRefTable takes a readSeeker and generates a Context only with the xRefTable created without optimization and objects reading
func ReadXRefTable(rs io.ReadSeeker, fileName string, fileSize int64, config *Configuration) (*SplitData, error) {

	log.Read.Println("ReadXRefTable: begin")

	ctx, err := NewContext(rs, fileName, fileSize, config)
	if err != nil {
		return nil, err
	}

	if ctx.Reader15 {
		log.Info.Println("ReadXRefTable: PDF Version 1.5 conforming reader")
	} else {
		log.Info.Println("ReadXRefTable: PDF Version 1.4 conforming reader - no object streams or xrefstreams allowed")
	}

	// Populate xRefTable.
	err = readXRefTable(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "ReadXRefTable: readXRefTable failed")
	}

	err = dereferenceObjectsWithoutLoading(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "ReadXRefTable: dereferenceObjects failed")
	}

	pdfStruct := SplitData{fileIn: fileName, ctx: ctx}

	for objNr, o := range ctx.XRefTable.Table {
		if o.Object != nil {
			sd, ok := o.Object.(StreamDict)
			if ok && sd.Dict.Type() != nil && *sd.Dict.Type() == "XObject" && *sd.Dict.Subtype() == "Image" {
				pdfStruct.data = append(pdfStruct.data, &sd)
				pdfStruct.splitLength += *sd.StreamLength
				log.Debug.Printf("Image: offset (%d) length (%d)", sd.StreamOffset, *sd.StreamLength)
				continue
			}
			d, ok := o.Object.(Dict)
			if ok && d.Type() != nil && *d.Type() == "Font" {
				// Extract font data
				font, err := ExtractFontFromDict(ctx, d, objNr)
				if err != nil {
					log.Info.Printf("Error extracting font => %s", err.Error())
					continue
				}
				if font != nil {
					pdfStruct.data = append(pdfStruct.data, font)
					pdfStruct.splitLength += *font.StreamLength
					log.Debug.Printf("Font: offset (%d) length (%d)", font.StreamOffset, *font.StreamLength)
				}
			}
		}
	}
	sort.Slice(pdfStruct.data, func(i, j int) bool {
		return pdfStruct.data[i].StreamOffset < pdfStruct.data[j].StreamOffset
	})

	pdfStruct.inMemory = true
	pdfStruct.Reset()

	log.Read.Println("ReadXRefTable: end")

	return &pdfStruct, nil
}

// Dereferences all objects including compressed objects from object streams.
func dereferenceObjectsWithoutLoading(ctx *Context) error {

	log.Read.Println("dereferenceObjects: begin")

	// Get sorted slice of object numbers.
	// TODO Skip sorting for performance gain.
	var keys []int
	for k := range ctx.XRefTable.Table {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, objNr := range keys {
		err := dereferenceObjectWithoutLoading(ctx, objNr)
		if err != nil {
			return err
		}
	}

	log.Read.Println("dereferenceObjects: end")

	return nil
}

func dereferenceObjectWithoutLoading(ctx *Context, objNr int) error {

	xRefTable := ctx.XRefTable
	xRefTableSize := len(xRefTable.Table)

	log.Read.Printf("dereferenceObject: begin, dereferencing object %d\n", objNr)

	entry := xRefTable.Table[objNr]

	if entry.Free {
		log.Read.Printf("free object %d\n", objNr)
		return nil
	}

	if entry.Compressed {
		err := decompressXRefTableEntry(xRefTable, objNr, entry)
		if err != nil {
			return err
		}
		//log.Read.Printf("dereferenceObject: decompressed entry, Compressed=%v\n%s\n", entry.Compressed, entry.Object)
		return nil
	}

	// entry is in use.
	log.Read.Printf("in use object %d\n", objNr)

	if entry.Offset == nil || *entry.Offset == 0 {
		log.Read.Printf("dereferenceObject: already decompressed or used object w/o offset -> ignored")
		return nil
	}

	o := entry.Object

	// Already dereferenced stream dict.
	if o != nil {
		logStream(entry.Object)
		updateBinaryTotalSize(ctx, o)
		log.Read.Printf("handleCachedStreamDict: using cached object %d of %d\n<%s>\n", objNr, xRefTableSize, entry.Object)
		return nil
	}

	// Dereference (load from disk into memory).

	log.Read.Printf("dereferenceObject: dereferencing object %d\n", objNr)

	// Parse object from file: anything goes dict, array, integer, float, streamdicts...
	o, err := ParseObject(ctx, *entry.Offset, objNr, *entry.Generation)
	if err != nil {
		return errors.Wrapf(err, "dereferenceObject: problem dereferencing object %d", objNr)
	}

	entry.Object = o

	// Linearization dicts are validated and recorded for stats only.
	err = handleLinearizationParmDict(ctx, o, objNr)
	if err != nil {
		return err
	}

	// Handle stream dicts.

	if _, ok := o.(ObjectStreamDict); ok {
		return errors.Errorf("dereferenceObject: object stream should already be dereferenced at obj:%d", objNr)
	}

	if _, ok := o.(XRefStreamDict); ok {
		return errors.Errorf("dereferenceObject: xref stream should already be dereferenced at obj:%d", objNr)
	}

	if sd, ok := o.(StreamDict); ok {

		// Dereference stream length if stream length is an indirect object.
		if sd.StreamLength == nil {
			if sd.StreamLengthObjNr == nil {
				return errors.New("LoadEncodedStreamContent: missing streamLength")
			}
			// Get stream length from indirect object
			sd.StreamLength, err = int64Object(ctx, *sd.StreamLengthObjNr)
			if err != nil {
				return err
			}
			log.Read.Printf("LoadEncodedStreamContent: new indirect streamLength:%d\n", *sd.StreamLength)
		}

		entry.Object = sd
	}

	log.Read.Printf("dereferenceObject: end obj %d of %d\n<%s>\n", objNr, xRefTableSize, entry.Object)

	logStream(entry.Object)

	return nil
}
